<?php
function wpst_register_widgets_api()
{
    register_widget('wpst_WP_Widget_Videos_Api');
}

add_action('widgets_init', 'wpst_register_widgets_api');

class wpst_WP_Widget_Videos_Api extends WP_Widget
{
    public function __construct()
    {
        $widget_options = array(
            'classname' => 'widget_videos_block',
            'description' => __('API VIDEO LIST', 'wpst'),
        );
        parent::__construct('widget_videos_api', 'RetroTube - Video Api', $widget_options);
    }

    public function widget($args, $instance)
    {
        extract($args);
        $title = $instance['title'];
        $url = isset($instance['url']) ? esc_attr($instance['url']) : '';
        $number_cat = isset($instance['number_cat']) ? esc_attr($instance['number_cat']) : '';
        $number_post = isset($instance['number_post']) ? esc_attr($instance['number_post']) : '3';
        echo $before_widget;
        if ($title) :
            echo $before_title . $title . $after_title;
            echo do_shortcode('[list_post_shortcode web="' . $url . '" cat_id=' . $number_cat . ' limit=' . $number_post . ']')

            ?>
            <div class="clear"></div>
            <?php
            echo $after_widget;
        endif;
    }

    public function form($instance)
    {
        $instance = wp_parse_args((array)$instance, array('title' => ''));
        $title = isset($instance['title']) ? esc_attr($instance['title']) : '';
        $url = isset($instance['url']) ? esc_attr($instance['url']) : '';
        $number_cat = isset($instance['number_cat']) ? esc_attr($instance['number_cat']) : '';
        $number_post = isset($instance['number_post']) ? esc_attr($instance['number_post']) : '';
        ?>
        <p><label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title', 'wpst'); ?> :</label>
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>"
                   name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>"/></p>
        <p><label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('URL', 'wpst'); ?> :</label>
            <input class="widefat" id="<?php echo $this->get_field_id('url'); ?>"
                   name="<?php echo $this->get_field_name('url'); ?>" type="text" value="<?php echo $url; ?>"/></p>
        <p><label for="<?php echo $this->get_field_id('number_cat'); ?>"><?php _e('Number Category', 'wpst'); ?>
                :</label>
            <input class="widefat" id="<?php echo $this->get_field_id('number_cat'); ?>"
                   name="<?php echo $this->get_field_name('number_cat'); ?>" type="text"
                   value="<?php echo $number_cat; ?>"/></p>
        <p><label for="<?php echo $this->get_field_id('number_post'); ?>"><?php _e('Number Post', 'wpst'); ?> :</label>
            <input class="widefat" id="<?php echo $this->get_field_id('number_post'); ?>"
                   name="<?php echo $this->get_field_name('number_post'); ?>" type="text"
                   value="<?php echo $number_post; ?>"/></p>
        <?php
    }

    public function update($new_instance, $old_instance)
    {
        $instance = $old_instance;
        $instance['title'] = isset($new_instance['title']) ? strip_tags($new_instance['title']) : '';
        $instance['url'] = isset($new_instance['url']) ? strip_tags($new_instance['url']) : '';
        $instance['number_cat'] = isset($new_instance['number_cat']) ? strip_tags($new_instance['number_cat']) : '';
        $instance['number_post'] = isset($new_instance['number_post']) ? strip_tags($new_instance['number_post']) : '';
        return $instance;
    }


}