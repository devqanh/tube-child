<?php
define('time_expiration_qa', 6*3600);
require_once('template-parts/list-post-api.php');
require_once('inc/widget-api-video.php');
add_action('wp_ajax_get_embed_aq', 'get_embed_aq_function');
add_action('wp_ajax_nopriv_get_embed_aq', 'get_embed_aq_function');
if (!function_exists('get_embed_aq_function')):
    function get_embed_aq_function()
    {
        global $post;
        $id = (isset($_POST['get_id'])) ? esc_attr($_POST['get_id']) : '';
        $server = (isset($_POST['server'])) ? esc_attr($_POST['server']) : '';
        $get_embed = get_field('server', $id)[$server]['embed'];
        echo $get_embed;
        die();
    }
endif;
define('HK_VNEXPRESS_PLUGIN_URL', get_stylesheet_directory_uri());
add_action('wp_enqueue_scripts', 'enqueue_parent_styles');

function enqueue_parent_styles()
{
    wp_enqueue_style('parent-style', get_template_directory_uri() . '/style.css');
}

add_filter('use_block_editor_for_post', '__return_false');
//  wpst_admin_scripts 
add_action('admin_menu', 'wpst_admin_scripts');
// wpst_scripts 
add_action('wp_enqueue_scripts', 'wpst_scripts');

// api list post remote    /wp-json/getlist/v1/list-cat/
add_action('rest_api_init', function () {
    register_rest_route('getlist/v1', 'list-cat/(?P<category_id>\d+)', array(
        'methods' => 'GET',
        'callback' => 'get_post_api_qa'
    ));
});
if (!function_exists('get_post_api_qa')):
    function get_post_api_qa($request)
    {

        // get the posts
        $limit_post = !empty($_GET['limit']) ? $_GET['limit'] : 10;
        $cat_id = !empty($request['category_id']) ? $request['category_id'] : 1;
        $posts_list = get_posts(array('type' => 'post', 'posts_per_page' => $limit_post, 'cat' => $cat_id));
        $post_data = array();

        if (!empty($posts_list)) :
            foreach ($posts_list as $posts) {

                $post_id = $posts->ID;
                $post_data[$post_id]['title'] = substr($posts->post_title, 0, 35);
                $post_data[$post_id]['permalink'] = $posts->guid;
                $post_data[$post_id]['thumbnail'] = get_the_post_thumbnail_url($post_id, xbox_get_field_value('wpst-options', 'main-thumbnail-quality'));
                $post_data[$post_id]['view'] = get_post_meta($post_id, 'post_views_count')[0];
                $post_data[$post_id]['duration'] = date('H:i:s', intval(get_post_meta($post_id, 'duration', true)));
            }
            $data = array('status' => true, 'data' => $post_data);
        else:
            $data = array('status' => false, 'data' => 'Not found');
        endif;
        wp_reset_postdata();

        return rest_ensure_response($data);
    }

// shortcode get list
endif;



