<?php

if (!function_exists('list_post_shortcode')):
    function list_post_shortcode($args)
    {

        ob_start();
        $limit = isset($args['limit']) ? $args['limit'] : 3;
        $request_url = $args['web'] . "/wp-json/getlist/v1/list-cat/" . $args['cat_id'] . "?limit=" . $limit;
        $request = wp_remote_get($request_url);

        if (is_wp_error($request)) {
            return false; // Bail early
        }

        $request_cache = str_replace(array('http://', 'https://', ':', '/', '?', '='), '_', $request_url);

        if (empty(get_transient('list_post_shortcode' . $request_cache))):
            $body = wp_remote_retrieve_body($request);
            set_transient('list_post_shortcode' . $request_cache, $body, time_expiration_qa);
        else :
            $body = get_transient('list_post_shortcode' . $request_cache);
        endif;

        $data = json_decode($body, true);
        ?>
        <div class="videos-list">
            <?php
            if ($data['status'] == true) :
                if (count($data['data']) > 0) :
                    foreach ($data['data'] as $key => $kq) :
                        ?>
                        <article id="post-<?php echo $key; ?>"
                                 class="thumb-block post-<?php echo $key; ?> post type-post status-publish format-standard has-post-thumbnail hentry category">
                            <a href="<?php echo $kq['permalink']; ?>"
                               data-title="<?php echo $kq['title']; ?>"
                               title="<?php echo $kq['title']; ?>">

                                <!-- Trailer -->


                                <!-- Thumbnail -->
                                <div class="post-thumbnail thumbs-rotation" data-thumbs="">
                                    <?php if ($kq['thumbnail']) : ?>
                                        <img src="<?php echo $kq['thumbnail']; ?>"/>
                                    <?php else:
                                        echo '<div class="no-thumb"><span><i class="fa fa-image"></i> ' . esc_html__('No image', 'wpst') . '</span></div>';
                                    endif; ?>
                                    <span class="views"><i class="fa fa-eye"></i> <?php echo $kq['view']; ?></span>
                                </div>


                                <div class="rating-bar no-rate" style="width: 100%">
                                    <div class="rating-bar-meter" style="width: 0%;"></div>
                                    <i class="fa fa-clock-o" aria-hidden="true"></i>
                                    <span><?php echo $kq['duration']; ?></span>
                                </div>

                                <header class="entry-header">
                                    <span><?php echo $kq['title']; ?></span>
                                </header><!-- .entry-header -->
                            </a>
                        </article><!-- #post-## -->
                    <?php
                    endforeach;
                endif;
            else :
                ?>
                <header class="entry-header">
                    <h1 class="page-title"><?php esc_html_e('Nothing found', 'wpst'); ?></h1>
                </header>
            <?php

            endif;
            ?>

        </div>
        <?php
        return ob_get_clean();
    }
endif;

add_shortcode('list_post_shortcode', 'list_post_shortcode');


add_shortcode('list_slider', function () {
    ob_start();
    ?>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.js"
            integrity="sha256-NXRS8qVcmZ3dOv3LziwznUHPegFhPZ1F/4inU7uC8h0=" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.css"
          integrity="sha256-UK1EiopXIL+KVhfbFa8xrmAWPeBjMVdvYMYkTAEv/HI=" crossorigin="anonymous"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick-theme.min.css"
          integrity="sha256-4hqlsNP9KM6+2eA8VUT0kk4RsMRTeS7QGHIM+MZ5sLY=" crossorigin="anonymous"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick-theme.css"
          integrity="sha256-etrwgFLGpqD4oNAFW08ZH9Bzif5ByXK2lXNHKy7LQGo=" crossorigin="anonymous"/>
    <div class="slider multiple-items">

        <article id="post-83"
                 class="thumb-block-custom post-83 post type-post status-publish format-standard has-post-thumbnail hentry category">
            <a href="http://localhost:8888/tube/?p=83" data-title="Maroon 5 - Girls Like You ft. Cardi"
               title="Maroon 5 - Girls Like You ft. Cardi">

                <!-- Trailer -->


                <!-- Thumbnail -->
                <div class="post-thumbnail thumbs-rotation" data-thumbs="">
                    <div class="no-thumb"><span><i class="fa fa-image"></i> No image</span></div>
                    <span class="views"><i class="fa fa-eye"></i> 0</span>
                </div>


                <div class="rating-bar no-rate" style="width: 100%">
                    <div class="rating-bar-meter" style="width: 0%;"></div>
                    <i class="fa fa-clock-o" aria-hidden="true"></i>
                    <span>10:00:00</span>
                </div>

                <header class="entry-header">
                    <span>Maroon 5 - Girls Like You ft. Cardi</span>
                </header><!-- .entry-header -->
            </a>
        </article><!-- #post-## -->
        <article id="post-84"
                 class="thumb-block-custom post-84 post type-post status-publish format-standard has-post-thumbnail hentry category">
            <a href="http://localhost:8888/tube/?p=83" data-title="Maroon 5 - Girls Like You ft. Cardi"
               title="Maroon 5 - Girls Like You ft. Cardi">

                <!-- Trailer -->


                <!-- Thumbnail -->
                <div class="post-thumbnail thumbs-rotation" data-thumbs="">
                    <div class="no-thumb"><span><i class="fa fa-image"></i> No image</span></div>
                    <span class="views"><i class="fa fa-eye"></i> 0</span>
                </div>


                <div class="rating-bar no-rate" style="width: 100%">
                    <div class="rating-bar-meter" style="width: 0%;"></div>
                    <i class="fa fa-clock-o" aria-hidden="true"></i>
                    <span>10:00:00</span>
                </div>

                <header class="entry-header">
                    <span>Maroon 5 - Girls Like You ft. Cardi</span>
                </header><!-- .entry-header -->
            </a>
        </article><!-- #post-## -->
        <article id="post-85"
                 class="thumb-block-custom post-85 post type-post status-publish format-standard has-post-thumbnail hentry category">
            <a href="http://localhost:8888/tube/?p=83" data-title="Maroon 5 - Girls Like You ft. Cardi"
               title="Maroon 5 - Girls Like You ft. Cardi">

                <!-- Trailer -->


                <!-- Thumbnail -->
                <div class="post-thumbnail thumbs-rotation" data-thumbs="">
                    <div class="no-thumb"><span><i class="fa fa-image"></i> No image</span></div>
                    <span class="views"><i class="fa fa-eye"></i> 0</span>
                </div>


                <div class="rating-bar no-rate" style="width: 100%">
                    <div class="rating-bar-meter" style="width: 0%;"></div>
                    <i class="fa fa-clock-o" aria-hidden="true"></i>
                    <span>10:00:00</span>
                </div>

                <header class="entry-header">
                    <span>Maroon 5 - Girls Like You ft. Cardi</span>
                </header><!-- .entry-header -->
            </a>
        </article><!-- #post-## -->
        <article id="post-86"
                 class="thumb-block-custom post-86 post type-post status-publish format-standard has-post-thumbnail hentry category">
            <a href="http://localhost:8888/tube/?p=83" data-title="Maroon 5 - Girls Like You ft. Cardi"
               title="Maroon 5 - Girls Like You ft. Cardi">

                <!-- Trailer -->


                <!-- Thumbnail -->
                <div class="post-thumbnail thumbs-rotation" data-thumbs="">
                    <div class="no-thumb"><span><i class="fa fa-image"></i> No image</span></div>
                    <span class="views"><i class="fa fa-eye"></i> 0</span>
                </div>


                <div class="rating-bar no-rate" style="width: 100%">
                    <div class="rating-bar-meter" style="width: 0%;"></div>
                    <i class="fa fa-clock-o" aria-hidden="true"></i>
                    <span>10:00:00</span>
                </div>

                <header class="entry-header">
                    <span>Maroon 5 - Girls Like You ft. Cardi</span>
                </header><!-- .entry-header -->
            </a>
        </article><!-- #post-## -->


    </div>
    <script>
        jQuery('.multiple-items').slick({
            infinite: true,
            autoplay: true,
            autoplaySpeed: 2000,
            dots: true,
            slidesToShow: 3,
            slidesToScroll: 3,
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        arrows: false
                    }
                }
            ]
        });
    </script>
    <style>
        .thumb-block-custom {
            padding: 5px;
            margin: 0;
        }

        .thumb-block-custom .post-thumbnail {
            line-height: 0;
            position: relative;
        }

        .thumb-block-custom .entry-header {
            font-weight: normal;
            text-align: center;
            height: 3.5em;
            overflow: hidden;
            color: #ddd;
            display: block;
            padding: 0.5em 0 0;
            font-size: 0.875em;
        }

        .slick-dots li button {
            background: none !important;
            color: #fff !important;
            border: none !important;
        }

        .slick-dots li.slick-active button:before {
            color: #fff !important;
        }

        .slick-arrow {
            background: transparent !important;
        }

        .thumb-block-custom .rating-bar-meter, .thumb-block-custom:hover .rating-bar i, .thumb-block-custom:hover .rating-bar span, .thumb-block-custom:hover .rating-bar.no-rate .rating-bar-meter {
            display: none;
        }
    </style>
    <?php
    return ob_get_clean();
});